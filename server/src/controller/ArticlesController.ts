import { Controller, Get } from '@overnightjs/core';
import express from 'express';
import articlesService from '@src/services/ArticlesService';

@Controller('api/articles')
export class ArticlesController {
    @Get('')
    public getArticles(request: express.Request, response: express.Response): void {
        response.send(articlesService.getArticles());
    }
}
