import * as bcrypt from 'bcrypt';
import { CreateUserDTO } from '@src/dto/user/CreateUserDTO';

class UserUtils {
    async isPasswordCorrect(actualPassword: string, expectedPassword: string) {
        return await bcrypt.compare(actualPassword, expectedPassword);
    }

    async encryptPassword(userDTO: CreateUserDTO) {
        userDTO.password = await bcrypt.hash(userDTO.password, 8);
    }
}

export default new UserUtils();
