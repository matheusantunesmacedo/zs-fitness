import express from 'express';
import Joi from 'joi';
import { StatusCodes } from 'http-status-codes';
import personalService from '@src/services/PersonalService';

const personalSchema: Joi.ObjectSchema = Joi.object({
    name: Joi.string().required(),
    contact: Joi.string().required(),
    speciality: Joi.string().required(),
    local: Joi.string().required(),
    description: Joi.string().required(),
    insta: Joi.string().required(),
    image: Joi.string(),
});

class PersonalRequestMiddleware {
    async validateRequest(request: express.Request, response: express.Response, next: express.NextFunction) {
        const { error } = personalSchema.validate(request.body);
        if (error) {
            response.status(StatusCodes.BAD_REQUEST).send({ error: error.details.map((x) => x.message).join(', ') });
        } else {
            next();
        }
    }
    async validateSamePersonalDoesntExist(request: express.Request, response: express.Response, next: express.NextFunction) {
        const personal = await personalService.getPersonalByContact(request.body.contact);
        if (personal) {
            response.status(StatusCodes.BAD_REQUEST).send({ error: `Personal already exists` });
        } else {
            next();
        }
    }
}

export default new PersonalRequestMiddleware();
