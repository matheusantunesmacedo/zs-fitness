export class PersonalCreateDTO {
    name: string;
    idade: number;
    description: string;
    contact: string;
    insta: string;
    local: string;
    image: string;
}
