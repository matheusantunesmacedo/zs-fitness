import * as nodemailer from 'nodemailer';
import emailConfig from '../config/emailConfig';

class EmailService {
    constructor(public to?: string, public subject?: string, public message?: string) {}

    sendMail() {
        const mailOptions = {
            from: 'portalband@band.com.br',
            to: this.to,
            subject: this.subject,
            html: this.message,
        };

        const transporter = nodemailer.createTransport({
            host: emailConfig.host,
            port: emailConfig.port,
            secure: false,
            auth: {
                user: emailConfig.user,
                pass: emailConfig.password,
            },
            tls: { rejectUnauthorized: false },
        });

        console.log(mailOptions);

        transporter.sendMail(mailOptions, function (error) {
            if (error) {
                return error;
            } else {
                return 'E-mail enviado com sucesso!';
            }
        });
    }
}
