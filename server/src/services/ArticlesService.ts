import * as data from '../database/articles.json';

class ArticlesService {
    getArticles() {
        return data.default;
    }
}
export default new ArticlesService();
