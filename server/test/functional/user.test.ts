describe('Root route simple test', () => {
    it('should return ok', async () => {
        const { body, status } = await global.testRequest.get('/');
        expect(status).toBe(200);
        expect(body).toStrictEqual({ ok: 'Working properly' });
    });
});
