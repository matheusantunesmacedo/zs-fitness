import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import images from '../../../assets/images';
import colors from '../../../assets/colors';

import {
  ArticlesCardsContainer,
} from '../../../assets/styles/components/articles';

const useStyles = makeStyles((theme) => ({
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function ArticlesCards(props) {
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const [state, setState] = useState({
    checkedA: true,
    checkedB: false,
  });

  const data = props.props;

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <ArticlesCardsContainer>
        <Card style={{ padding: '10px' }}>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe">
                {data.theme[0]}
              </Avatar>
            }
            title={data.title}
            subheader={data.theme}
          />
          <CardMedia
                component="img"
                image={data.image ? data.image : images.logo}
                height='200'
                title="image article"
              />
            <CardContent>
              <Typography variant="body2" color="textSecondary" component="p">
                  {data.contentPreview}
              </Typography>
            </CardContent>
            <CardActions disableSpacing>
              <Switch
                size="small"
                checked={state.checkedB}
                onChange={handleChange}
                color="primary"
                name="checkedB"
                inputProps={{ 'aria-label': 'primary checkbox' }}
              />
              <IconButton
                className={clsx(classes.expand, {
                  [classes.expandOpen]: expanded,
                })}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
              >
                <ExpandMoreIcon />
              </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
              {state.checkedB
                ?
                <CardContent style={{ background: `${colors.secondary}`, color: `${colors.primary}` }} >
                  <Typography paragraph>
                    {data.content}
                  </Typography>
                </CardContent>
                :
                <CardContent style={{ background: `${colors.primary}`, color: `${colors.secondary}` }} >
                <Typography paragraph>
                  {data.content}
                </Typography>
              </CardContent>
              }
              
            </Collapse>
        </Card>
      </ArticlesCardsContainer> 
  );
}
