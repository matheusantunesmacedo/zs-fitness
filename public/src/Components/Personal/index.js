import React, { useEffect, useState } from 'react';

import PersonalCard from './CardsPersonal';
import { ContainerCards, SearchContainer, ContainerCategories } from '../../assets/styles/components/personal';
import { SearchField } from '../../assets/styles/components/inputs';



import api from '../../services/api';

function Personal() {

  const [input, setInput] = useState('');
  const [keyword, setKeyword] = useState('');
  const [items, setItems] = useState([]);
  const [defaultItems, setDefaultItems] = useState([]);

  

  const updateInput = (input) => {
    const filtered = defaultItems.filter(names => {
      console.log(names.names)
      return names.speciality.toLowerCase().includes(input.toLowerCase())
    })
    setInput(input);
    console.log(filtered)
    setItems(filtered);
  }


  async function getPersonals() {
    try {
      const { data } = await api.get('api/personal')
      setDefaultItems(data)
      setItems(data)
    } catch (error) {
      alert('Ocorreu um erro ao buscar os items')
    }
  }

  useEffect(() => {
    getPersonals()
  }, [])


  return (
    <div>
      <SearchContainer>
        <SearchField id="search-input" info="personal" input={input} value={keyword} onChange={(search) => {
            setKeyword(search.target.value)
            updateInput(search.target.value) }}/>
      </SearchContainer>
      <ContainerCards>
        {items.map((item) => {
          if(item){
          return <PersonalCard key={item.id} props={item} />
          } else {
            return null
          }
        })}
      </ContainerCards>
    </div>
  )
}

export default Personal
