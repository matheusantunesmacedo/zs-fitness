import React, { useState } from 'react';

import '../../assets/styles/components/header/items.css';
import { Bars, CancelButton, LinkMenu } from '../../assets/styles/components/header';

import { withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountBoxIcon from '@material-ui/icons/AccountBox';

import colors from '../../assets/colors';

import {
  HeaderMain,
  ContainerLogo, 
  ContainerImgLogo,
  HeaderLogoImg,
  HeaderLogoText,
  NavMain,
  ListLinks,
  NavList,
  HeaderBtnsLogin,
  LinkButton,
  ButtonHeader,
  ButtonMobile,
  LinkPerfil
} from '../../assets/styles/components/header';

import images from '../../assets/images';

import api from '../../services/api';

const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: `${colors.greenLight}`,
    color: `${colors.greenLight}`,
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2)',
      opacity: 0,
    },
  },
}))(Badge);

function Header() {
  const url_atual = window.location.href.split('/')[3];
  const [url, setUrl] = useState('/' + url_atual[1]);

  const [anchorEl, setAnchorEl] = useState(null);
  const [clicked, setClicked] = useState(false);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick1 = () => {
    setClicked(!clicked);
    return clicked;
  }

  const [personal, setPersonal] = useState({});

  async function getPersonal() {
    try {
      const id = localStorage.getItem('id');
      const response = await api.get(`/api/personal/single/${id}`);
      setPersonal(response.data);
    } catch (error) {
      alert(error);
    }
  }

  getPersonal(() => {
    getImage();
  }, [])

  return (
    <HeaderMain>
        <ContainerLogo>
          <ContainerImgLogo href="/">
            <HeaderLogoImg  className="header-logo" src={images.logo}>
            </HeaderLogoImg>
          </ContainerImgLogo>
          <HeaderLogoText className="header-logo-text">
            ZS Fitness
          </HeaderLogoText>
        </ContainerLogo>
      <NavMain >
        <ListLinks className={clicked ? "header-menu active" : "header-menu"}>
          <NavList>
            <LinkMenu 
              className="nav-mobile"
              to="/" 
              onClick={() => setUrl(" ")}
              style={{ backgroundColor: '/' + url_atual === "/" ? colors.aux : colors.secondary }}
            >
              Home
            </LinkMenu>
          </NavList>

          <NavList>
            <LinkMenu 
              className="nav-mobile"
              to="/Personal"
              onClick={() => setUrl("/Personal")}
              style={{ backgroundColor: '/' +url_atual === "/Personal" ? colors.aux : colors.secondary }}
            >Professores</LinkMenu>
          </NavList>
          
          <NavList>
            <LinkMenu
              className="nav-mobile" 
              to="/Exercises" 
              onClick={() => setUrl("/Exercices")}
              style={{ backgroundColor: '/' + url_atual === "/Exercises" ? colors.aux : colors.secondary}}
            >Exercícios</LinkMenu>
          </NavList>

          <NavList>
            <LinkMenu 
              className="nav-mobile"
              to="/Articles"
              onClick={() => setUrl("/Articles")}
              style={{ backgroundColor: '/' + url_atual === "/Articles" ? colors.aux : colors.secondary}}
            >Artigos</LinkMenu>
          </NavList>
        </ListLinks>
      </NavMain>
      {localStorage.getItem('token')
        ?
        <>
          <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
            <StyledBadge
              className="perfil"
              overlap="circle"
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              variant="dot"
            >
            <Avatar src={personal.image ? personal.image : "/broken-image.jpg"} />
            </StyledBadge> 
          </Button>
          {localStorage.getItem('id')
            ?
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>
                <ExitToAppIcon style={{ fontSize: "medium", margin: '5px' }} />
                <LinkPerfil 
                  onClick={() => localStorage.clear()}
                  href="/"
                >
                Sair
                </LinkPerfil>
              </MenuItem>
            </Menu>
            :
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
            <MenuItem onClick={handleClose}>
              <AccountBoxIcon style={{ fontSize: "medium", margin: '5px' }} />
              <LinkPerfil href="/Account"
                onClick={() => setUrl("/Account")}
              >
                Minha conta
              </LinkPerfil>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <ExitToAppIcon style={{ fontSize: "medium", margin: '5px' }} />
              <LinkPerfil 
                onClick={() => localStorage.clear()}
                href="/"
              >
              Sair
              </LinkPerfil>
              </MenuItem>
            </Menu>
            
          }
          </>
        :
        <HeaderBtnsLogin>
          <ButtonHeader>
            <LinkButton href="Login">Entrar / Registrar</LinkButton>
          </ButtonHeader>
        </HeaderBtnsLogin>
      }
      <ButtonMobile className="header-icon" onClick={handleClick1}>
        {clicked ? <CancelButton style={{ width: '25px' }} /> : <Bars style={{ width: '20px' }} />}
      </ButtonMobile>
  </HeaderMain>
  );
}

export default Header;


    