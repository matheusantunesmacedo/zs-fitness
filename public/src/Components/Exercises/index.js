import React, { useEffect, useState } from 'react';

import ExercisesCard from './CardsExercise'
import { ExerciseContainer, SearchContainer } from '../../assets/styles/components/exercises';
import { SearchField } from '../../assets/styles/components/inputs';

import api from '../../services/api';

function Exercises() {
  const [exercises, setExercises] = useState([]);
  const [search, setSearch] = useState('');

  async function handleSearch() {
    try {
      if (search && search !== '') {
        const regex = new RegExp(search, 'gi')
        const filter = exercises.filter(({ name, member }) => {
          return regex.test(name) || regex.test(member)
        })
        setExercises(filter);
      } else {
        getExercises();
      }
    } catch (error) {
      alert('Ocorreu um erro ao buscar os items');
    }
  }

  async function getExercises() {
    try {
      const { data } = await api.get('api/exercices');
      setExercises(data);
    } catch (error) {
      alert('Ocorreu um erro ao buscar os items');
    }
  }

  useEffect(() => {
    getExercises();
  }, [])

  useEffect(() => {
    handleSearch();
  }, [search])

  return (
    <div>
      <SearchContainer>
        <SearchField id="search-input" info="exercício" onChange={(event) => setSearch(event.target.value)} />
      </SearchContainer>
      <ExerciseContainer>
        {exercises.map((item) => {
          return <ExercisesCard key={item.id} props={item} />
        })}
      </ExerciseContainer>
    </div>
  )
}

export default Exercises
