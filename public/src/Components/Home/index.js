import React, { useState, useEffect } from 'react';

import Carousel from 'react-material-ui-carousel';
import ButtonBase from '@material-ui/core/ButtonBase';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import {
  HomeImg,
  HomeContainerImg,
  HomeText,
  HomeContainer,
  HomeDescriptions,
  HomeSlideInstructor,
  HomeInstructors,
  Container,
} from '../../assets/styles/components/home';

import colors from '../../assets/colors';
import images from '../../assets/images';

import api from '../../services/api';

function LayoutHomeImg(props) {
  return (
    <div>
      <h1 style={{ fontSize: '1rem', display: 'flex', justifyContent: 'center', marginTop: '20px', marginBottom: '5px' }}>{props.item.name}</h1>
      <h2 style={{ fontSize: '1rem', display: 'flex', justifyContent: 'center', marginBottom: '20px' }}>
        {props.item.speciality}
      </h2>
      <HomeImg src={props.item.image ? props.item.image : images.logo} />
      <h2 style={{ fontSize: '1em', display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
        {props.item.local}
        -
        {'PE'}
      </h2>
    </div>
  );
}

function LayoutHomeDescription(props) {
  return (
    <HomeDescriptions>
      {props.item.description}
    </HomeDescriptions>
  );
}

function Home() {
  const items = [
    {
      image: `${images.homeImage.image1}`,
      title: 'Venha fazer parte você também!',
      width: '100%',
    },
    {
      image: `${images.homeImage.image2}`,
      title: 'Venha fazer parte você também!',
      width: '100%',
    },
    {
      image: `${images.homeImage.image3}`,
      title: 'Venha fazer parte você também!',
      width: '100%',
    },
  ];

  const descriptions = [
    {
      description: 'Consegui através da plataforma,o contato de um bom profissional para ter um novo estilo de vida mais saúdavel'
    },
    {
      description: 'O professor que encontrei dentro da plataforma foi sempre muito solícito e me ajudou a emagrecer mais de 20 kgs e hoje sou uma pessoa mais feliz'
    },
    {
      description: 'Não conhecia a ZS-Fitness até que fui apresentado através de um amigo próximo.Foi o melhor lugar que poderia achar pra conseguir ter uma vida mais saudável e sabendo que há bons profissionais dentro da plataforma,além de artigos sobre vida fitness.Gostei demais!!'
    },
  ];

  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      minWidth: 300,
      width: '100%',
    },
    image: {
      position: 'relative',
      height: '100vh',
      [theme.breakpoints.down('xs')]: {
        width: '100% !important', 
        height: 100,
      },
      '&:hover, &$focusVisible': {
        zIndex: 1,
        '& $imageBackdrop': {
          opacity: 0.15,
        },
        '& $imageMarked': {
          opacity: 0,
        },
        '& $imageTitle': {
          border: '2px solid currentColor',
        },
      },
    },
    focusVisible: {},
    imageButton: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      color: `${colors.primary}`,
    },
    imageSrc: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundSize: 'cover',
      backgroundPosition: 'center 40%',
    },
    imageBackdrop: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: `${colors.secondary}`,
      opacity: 0.4,
      transition: theme.transitions.create('opacity'),
    },
    imageTitle: {
      position: 'relative',
      padding: '15px',
    },
    imageMarked: {
      height: 2,
      width: 15,
      backgroundColor: theme.palette.common.white,
      position: 'absolute',
      bottom: -2,
      left: 'calc(50% - 9px)',
      transition: theme.transitions.create('opacity'),
    },
  }));

  const classes = useStyles();

  const [topPersonal, setTopPersonal] = useState([]);

  async function getTopPersonals()  {
    try {
      const response = await api.get('/api/personal/top3');
      setTopPersonal(response.data); 
    } catch (error) {
      alert(error);
    }
  }

  useEffect(() => {
    getTopPersonals();
  }, [])

  return (
    <div>
      <HomeContainerImg>
        <Carousel>
          {
              items.map((item, i) => 
              <ButtonBase
                item={item}
                href="Login"
                focusRipple
                key={item.title}
                className={classes.image}
                focusVisibleClassName={classes.focusVisible}
                style={{
                  width: item.width,
                }}
              >
                <span
                  className={classes.imageSrc}
                  style={{
                    backgroundImage: `url(${item.image})`,
                  }}
                />
                <span className={classes.imageBackdrop} />
                <span className={classes.imageButton}>
                  <Typography
                    component="span"
                    variant="subtitle1"
                    color="inherit"
                    className={classes.imageTitle}
                    style={{ fontSize: '15px' }}
                  >
                    {item.title}
                    <span className={classes.imageMarked} />
                  </Typography>
                </span>
              </ButtonBase> 
              )
          }
        </Carousel>
      </HomeContainerImg>
      <HomeText>
        Uma Plataforma Open Source que tem o objetivo de proporcionar
        mais saúde às pessoas através de exercícios com auxílio de
        treinadores qualificados!
      </HomeText>
      <hr style={{ marginBottom: '30px', marginTop: 0 }} />
      <div style={{ marginBottom: '30px', opacity: 0.7 }}>
        <Carousel>
          {
            descriptions.map((item, i) => <LayoutHomeDescription key={i} item={item} />)
          }
        </Carousel>
      </div>
      <HomeContainer>
        <img className="foto" src={images.homeImage.image4} alt="artigos" style={{ width: '100%', marginBottom: '0px' }} />
      </HomeContainer>
      <HomeInstructors>Nossos Instrutores</HomeInstructors>
      <Container>
        <HomeSlideInstructor>
          <Carousel>
            {
              topPersonal.map((item, i) => <LayoutHomeImg key={i} item={item} />)
            }
          </Carousel>
        </HomeSlideInstructor>
      </Container>
    </div>
  );
}

export default Home;
