import styled from 'styled-components';

import colors from '../../../colors';

export const HomeContainerImg = styled.div`
  background-color: ${colors.secondary};
  
  width: 100vw;
  margin: 0 auto;  
  padding: 0 0 15px 0;
  
  box-shadow: 0px 1px 3px whitesmoke;
`;

export const HomeContainerMain = styled.h1`
  display: flex;
  justify-content: center;
`;

export const HomeContainer = styled.div`
  width: 100vw;
`;

export const HomeImg = styled.img`
    width: 100vw;
    height: 100%;
    margin: 0 auto;
    transition: 1s ease;

    &:hover {
      opacity: 0.6;
    }
`;

export const HomeDescriptions = styled.p`
    width: 40%;
    margin: 0 auto;
    font-size: 1.5em;
    font-family: 'Raleway', sans-serif;
`;

export const HomeText = styled.p`
    width: 100vw;
    margin: 0 0 30px 0;
    padding: 20px 0;
    font-family: 'Raleway', sans-serif;
    font-size: 1.1rem;
    text-align: center;
    font-weight: bold;
    
    color: ${colors.secondary};
    background-color: ${colors.primary};
    background-color: #ccc;
    opacity: 0.8;
    transition: 0.7s;

    &:hover {
      opacity: 1;
      font-weight: normal;
      background-color: ${colors.secondary};
      color: ${colors.primary};
    }
`;

export const HomeInstructors = styled.h1`
     display: flex;
     justify-content: center;
     
     width: 100vw;
     font-size: 45px;
     padding: 20px 0;
     margin-top: 0;
     margin-bottom: 0;
     font-family: 'Raleway', sans-serif;
     font-weight: lighter;
     
     background-color: ${colors.primary};
     opacity: 0.8;
`;

export const HomeSlideInstructor = styled.div`
    display: flex;
    width: 25%;
    margin: auto;
    margin-bottom: 0;
    font-family: 'Raleway', sans-serif;
    border: 3px solid ${colors.secondary};
    
    color: ${colors.secondary};
    background-color: ${colors.primary};
    transition: 0.7s;
    border-radius: 10px;

    &:hover {
      background-color: ${colors.secondary};
      color: ${colors.primary};
    }
    & img {
      display: flex;
      width: 70%;
      justify-content: center;
      border-radius: 10px;
    }
    & img:hover {
      border: 1px solid ${colors.primary};
    } 
`;

export const Container = styled.div`
    padding: 30px;
    width: 100vw;
    background: black;
  
    transition: 1s linear;
    transition-delay: 0.5s;

    &:hover {
      background: ${colors.secondary};

}`;

export const ButtonBase = styled.button`
    transition: 1s linear;
    transition-delay: 0.5s;

    &:hover {
      background: ${colors.primary};
      color: ${colors.secondary}

}`;
