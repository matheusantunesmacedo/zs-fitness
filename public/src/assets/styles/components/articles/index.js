import styled from 'styled-components';

export const ArticleContainer = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow-x: hidden;
`;

export const ArticlesCardsContainer = styled.div`
  width: 60%;
  margin: 20px 0;
`;
