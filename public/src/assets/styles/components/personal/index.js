import styled from 'styled-components'

export const ContainerCards = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: auto auto auto;
  justify-content: space-around;
`

export const ContainerCard = styled.div`
  inline-size: 80%;
  margin: auto;
`

export const CardContacts = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`

export const CardImgContact = styled.img`
  height: 1.2rem;
  margin: 0.6rem;
  text-align: center;
  justify-content: left;
`

export const ContainerCategories = styled.div`
  display:flex;
  flex-direction: row;
  align-items:center;
`

export const CardLink = styled.a`
  :hover {
    zoom: 110%;
  }
`
export const SearchContainer = styled.div`
  width: 100%;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 20px 0 0 0;
`
