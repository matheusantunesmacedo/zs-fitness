import styled from 'styled-components';

import colors from '../../../colors';

export const FooterContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  width: 100%;
  height: 20%;
  background-color: ${colors.secondary};
  bottom: 0;
  box-shadow: 5px 0 5px ${colors.secondary};
`;

export const ContainerContact = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: center;
`;

export const FooterContainerContactNet = styled.div`
  display: flex;
  flex-direction: row;
  text-align: center;
`;

export const FooterContainerContact = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;

  @media(max-width: 1000px) {
    display: flex;
    flex-direction: column;
    margin: 10px;
    justify-content: center;
  }
`;

export const FooterContainerContactText = styled.p`
  display: flex;
  align-items: center;
  font-size: ${(props) => (props.fontSize ? props.fontSize : '.65rem')};
  color: ${colors.primary};
  margin: 5px;
`;

export const FooterContainerContactImg = styled.img`
  display: flex;
  flex-direction: column;
  height: 1.3rem;
  margin: 5px;
  text-align: center;
  justify-content: center;
`;

export const ContainerLogo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const FooterLogoImg = styled.img`
  display: flex;
  justify-content: center;
  height: 40px;
  width: 40px; 
  margin: 0 auto;
`;

export const FooterLogoText = styled.p`
  display: flex;
  align-items: center;
  justify-content: center; 
  font-size: 15px;
  font-weight: bold;
  color: ${colors.aux};
  font-family: 'Raleway', sans-serif;
  margin: 8px auto;
`;

export const FooterText = styled.p`
  display: flex;
  align-items: center;
  justify-content: center; 
  font-size: 10px;
  width: 350px;
  color: ${colors.primary};
  font-family: 'Raleway', sans-serif;
  text-align: justify;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 10px;
`;

