import React from 'react';


import { BrowserRouter, Switch, Route } from 'react-router-dom';



import Header from './Components/Header';
import Personal from './Components/Personal';
import Home from './Components/Home';
import Articles from './Components/Articles';
import Login from './Components/Login';
import Register from './Components/Register';
import Exercises from './Components/Exercises';
import Account from './Components/Account';
import ErrorPage from './Components/ErrorPage';
import Footer from './Components/Footer';

function Routes() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/Articles" component={Articles} />
        <Route path="/Personal" component={Personal} />
        <Route path="/Exercises" component={Exercises} />
        <Route path="/Login" component={Login} />
        <Route path="/Register" component={Register} />
        <Route path="/Account" component={Account} />
        <Route component={() => <ErrorPage />} />
      </Switch>
      <Footer />
    </BrowserRouter>
  );
}

export default Routes;
